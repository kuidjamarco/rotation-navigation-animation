import dog from './image.jpg'
import './App.css';

function rotate(){

  document.querySelector('.circle').classList.add("marge");
  document.querySelector('.fa-bars').classList.add('fmarge');
  document.querySelector('.fa-times').classList.add('tmarge');
  document.querySelector('.fa-user').classList.add('user');
  document.querySelector('.fa-envelope').classList.add('envelope');
  document.querySelector('.menu').classList.add('link');

  //adding effet class to the rotating body
  const rotate = document.querySelector('.rotating-body'); 
  rotate.classList.add("rotation");
}
function rotateBack(){
  //adding effet class to the rotating body
  const rotate = document.querySelector('.rotating-body'); 
  rotate.classList.remove("rotation");
  
  document.querySelector('.menu').classList.remove('link');
  document.querySelector('.circle').classList.remove("marge");
  document.querySelector('.fa-bars').classList.remove('fmarge');
  document.querySelector('.fa-times').classList.remove('tmarge');
  document.querySelector('.fa-envelope').classList.remove('envelope');
  document.querySelector('.fa-user').classList.remove('user');
}

function App() {
  return (
    <div className="App">
      <div className="menu"> 
          <li><i className="fa fa-home" aria-hidden="true"> Home</i></li>
          <li><i className="fa fa-user" aria-hidden="true"> About</i></li>
          <li><i className="fa fa-envelope" aria-hidden="true"> Contact</i></li>
        </div>
      <div className="rotating-body">
        <div className="circle">
          <i className="fa fa-bars icon" onClick={rotate} aria-hidden="true"></i>
          <i className="fa fa-times active" onClick={rotateBack} aria-hidden="true"></i>
        </div>
        <div className="article">
          <div className="title">
            <h1>Amazing Article</h1>
            Florin pop
          </div>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium quia in ratione dolores cupiditate, maxime aliquid impedit dolorem nam dolor omnis atque fuga labore modi veritatis porro laborum minus, illo, maiores recusandae cumque ipsa quos. Tenetur, consequuntur mollitia labore pariatur sunt quia harum aut. Eum maxime dolorem provident natus veritatis molestiae cumque quod voluptates ab non, tempore cupiditate? Voluptatem, molestias culpa. Corrupti, laudantium iure aliquam rerum sint nam quas dolor dignissimos in error placeat quae temporibus minus optio eum soluta cupiditate! Cupiditate saepe voluptates laudantium. Ducimus consequuntur perferendis consequatur nobis exercitationem molestias fugiat commodi omnis. Asperiores quia tenetur nemo ipsa.
          </p>
          <div>
            <h4>My Dog</h4>
            <img src={dog} className="dog-img" alt="my dog" />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium quia in ratione dolores cupiditate, maxime aliquid impedit dolorem nam dolor omnis atque fuga labore modi veritatis porro laborum minus, illo, maiores recusandae cumque ipsa quos. Tenetur, consequuntur mollitia labore pariatur sunt quia harum aut. Eum maxime dolorem provident natus veritatis molestiae cumque quod voluptates ab non, tempore cupiditate?
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
